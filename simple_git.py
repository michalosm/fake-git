import os
import shutil
from os import path
from pathlib import Path
import sys

MAIN_GIT_DIRECTORY = 'git'
STAGING_DIRECTORY = f'{MAIN_GIT_DIRECTORY}/staged'
COMMIT_DIRECTORY = f'{MAIN_GIT_DIRECTORY}/commit'
SCRIPT_NAME = 'simple_git.py' 

class SimpleGit:

    def init(self):
        if not path.exists(MAIN_GIT_DIRECTORY):
            try:
                os.mkdir(MAIN_GIT_DIRECTORY)
                os.mkdir(STAGING_DIRECTORY)
                os.mkdir(COMMIT_DIRECTORY)
                self.add_all_files_to_staging()
                self.commit()
            except OSError:
                print ("Creation of the git repository failed") 

    def add(self,files):
        for f in files:
            if f!=MAIN_GIT_DIRECTORY and f!=SCRIPT_NAME and (self.is_modified(f) or self.is_new_file(f)):
                shutil.copyfile(f,f'{STAGING_DIRECTORY}/{f}')
        
    def commit(self):
        commit_number = self.get_current_number_of_commit()
        os.mkdir(f'{COMMIT_DIRECTORY}/{commit_number}')
        self.move_files_from_staging_to_commit(commit_number)

    def status(self):
       files = os.listdir('.')
       for file in files:
           self.print_status_of_file(file)        

    def move_files_from_staging_to_commit(self,commit_number):
        files = os.listdir(STAGING_DIRECTORY)
        for f in files:
            if f!= MAIN_GIT_DIRECTORY:
                os.replace(f'{STAGING_DIRECTORY}/{f}',f'{COMMIT_DIRECTORY}/{commit_number}/{f}')

    def add_all_files_to_staging(self):
        files = os.listdir('.')
        for file in files:
            if file!=MAIN_GIT_DIRECTORY and file!=SCRIPT_NAME:
                shutil.copyfile(file,f'{STAGING_DIRECTORY}/{file}')            

    def get_current_number_of_commit(self):
        max = 0
        for r, directories, files in os.walk(COMMIT_DIRECTORY):
            for d in directories:
                if int(d)>max:
                    max = int(d)
        return max+1

    def print_status_of_file(self,file): 
        if file ==MAIN_GIT_DIRECTORY or file==SCRIPT_NAME:
            pass
        elif file in os.listdir(STAGING_DIRECTORY):
            self.print_staging(file)
        elif self.is_new_file(file):
            self.print_new_file(file)
        elif self.is_modified(file):
            self.print_modified_file(file)
    
    def is_modified(self,file):
        path = Path(file)
        last_modified = path.stat().st_mtime  
        return last_modified>self.last_modified_time(file)

    def is_new_file(self,file):
        return not file in self.get_all_files()   

    def print_staging(self,file):
        print(f'> {file} (staged file)')

    def print_new_file(self,file):
        print(f'> {file} (new file)')

    def print_modified_file(self,file):    
        print(f'> {file} (modified file)')

    def get_all_files(self):
        all_files = []
        for r, d, files in os.walk(f'{COMMIT_DIRECTORY}'):
            for name in files:
                all_files.append(name)   
        return all_files        

    def last_modified_time(self,file):
        max = 0
        for root, dirs, files in os.walk(f'{COMMIT_DIRECTORY}'):
            for name in files:
                if name ==file:
                    path = Path(os.path.join(root, name))
                    if path.stat().st_mtime>max:
                        max = path.stat().st_mtime
        return max

if __name__=='__main__':
    git = SimpleGit()
    if (sys.argv[1]=='init'):
        git.init()
    if (sys.argv[1]=='commit'):
        git.commit()
    if (sys.argv[1]=='status'):
        git.status()
    if (sys.argv[1]=='add'):
        git.add(sys.argv[2:])            